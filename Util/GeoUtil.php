<?php
/**
 * Created by PhpStorm.
 * User: dees91
 * Date: 22/08/16
 * Time: 14:00
 */

namespace AppBundle\Util;


class GeoUtil {
	/**
	 * @param $angle
	 *
	 * @return int
	 */
	public static function getOppositeAngle($angle) {
		$opposite = $angle + 180;

		if ($opposite >= 360) {
			$opposite -= 360;
		}

		return $opposite;
	}

	/**
	 * @param $radius
	 *
	 * @return float
	 */
	public static function convertRadius($radius) {
		return $radius / 111;
	}

	/**
	 * @param $lat1
	 * @param $lng1
	 * @param $lat2
	 * @param $lng2
	 *
	 * @return float
	 */
	public static function distance($lat1, $lng1, $lat2, $lng2) {
		$theta = $lng1 - $lng2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;

		return $miles * 1.609344;
	}
}