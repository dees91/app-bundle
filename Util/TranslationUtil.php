<?php
/**
 * @author Piotr Krawczyk
 * Date: 29.10.15
 */

namespace AppBundle\Util;

use Symfony\Component\Translation\Loader\YamlFileLoader;
use Symfony\Component\Translation\Translator;

class TranslationUtil
{
	/**
	 * @var Translator
	 */
	protected static $instance;

	public static function trans($string, $locale = 'pl') {
		try {
			if (self::$instance == null) {
				self::$instance = new Translator($locale);
				self::$instance->addLoader('yaml', new YamlFileLoader());
				self::$instance->addResource('yaml', dirname(dirname(dirname(__DIR__))) . '/Resources/translations/errors.' . $locale . '.yml', $locale);
			}

			return self::$instance->trans($string);
		} catch (\Exception $e) {
			return $string;
		}
	}
}