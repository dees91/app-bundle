<?php
/**
 * Created by PhpStorm.
 * User: dees91
 * Date: 11/08/16
 * Time: 15:41
 */

namespace AppBundle\Util;


use Carbon\Carbon;

class DateUtil {

	/**
	 * @param        $data
	 * @param string $format
	 *
	 * @return bool
	 */
	public static function isValidFormat($data, $format = 'Y-m-d H:i:s') {
		return date($format, strtotime($data)) == $data;
	}

	/**
	 * @param        $days
	 * @param string $format
	 *
	 * @return bool|string
	 */
	public static function nowMinusDays($days, $format = 'Y-m-d H:i:s') {
		return date($format, strtotime(date('Y-m-d H:i:s') . ' -' . $days . ' days'));
	}

	/**
	 * @param string $format
	 *
	 * @return bool|string
	 */
	public static function now($format = 'Y-m-d H:i:s') {
		return date($format);
	}

	/**
	 * @param $dateFrom
	 * @param $dateTo
	 *
	 * @return array
	 */
	public static function getDatesFromRange($dateFrom, $dateTo) {
		$start = Carbon::createFromFormat('Y-m-d', $dateFrom);
		$end = Carbon::createFromFormat('Y-m-d', $dateTo);

		$dates = [];

		while ($start->lte($end)) {

			$dates[] = $start->copy()->format('Y-m-d');

			$start->addDay();
		}

		return $dates;
	}
}