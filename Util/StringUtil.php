<?php
/**
 * Created by PhpStorm.
 * User: dees91
 * Date: 30/05/16
 * Time: 14:21
 */

namespace AppBundle\Util;


class StringUtil {

	const TYPE_BOOLEAN = 1;
	const TYPE_DATE_FROM_TIMESTAMP = 2;
	const TYPE_ARRAY = 3;

	public static function getInt($data, $key, $default = 1) {
		return self::getValuable(isset($data[$key]) ? $data[$key] : $default);
	}

	public static function getValuable($string) {
		$string = trim($string);
		if (empty($string) && $string !== false && $string !== '0') {
			return null;
		}
		if (NumericUtil::isFloat($string)) {
			return floatval($string);
		}
		if (NumericUtil::isFloat(str_replace(',', '.', $string))) {
			return floatval(str_replace(',', '.', $string));
		}
		if (is_numeric($string) && strlen($string) < 10) {
			return intval($string);
		}
		if (strtolower($string) === 'true') {
			return true;
		}
		if (strtolower($string) === 'false') {
			return false;
		}
		if (strtolower($string) === 'null') {
			return null;
		}

		return $string;
	}

	public static function getTyped($string, $type) {
		switch ($type) {
			case self::TYPE_BOOLEAN: return (!is_null($string) ? (self::getValuable($string) ? true : false) : null);
			case self::TYPE_DATE_FROM_TIMESTAMP: return date('Y-m-d H:i:s', $string);
			case self::TYPE_ARRAY: return !empty($string) ? explode(',', str_replace(' ', '', $string)) : [];
			default: return self::getValuable($string);
		}
	}

	/**
	 * Determine if a given string ends with a given substring.
	 *
	 * @param string  $haystack
	 * @param string|array  $needles
	 * @return bool
	 */
	public static function endsWith($haystack, $needles)
	{
		foreach ((array) $needles as $needle)
		{
			if ($needle == substr($haystack, -strlen($needle))) return true;
		}

		return false;
	}

	/**
	 * Generate a more truly "random" alpha-numeric string.
	 *
	 * @param  int     $length
	 * @return string
	 *
	 * @throws \RuntimeException
	 */
	public static function random($length = 16)
	{
		if (function_exists('openssl_random_pseudo_bytes'))
		{
			$bytes = openssl_random_pseudo_bytes($length * 2);

			if ($bytes === false)
			{
				throw new \RuntimeException('Unable to generate random string.');
			}

			return substr(str_replace(array('/', '+', '='), '', base64_encode($bytes)), 0, $length);
		}

		return static::quickRandom($length);
	}

	/**
	 * Generate a "random" alpha-numeric string.
	 *
	 * Should not be considered sufficient for cryptography, etc.
	 *
	 * @param  int     $length
	 * @return string
	 */
	public static function quickRandom($length = 16)
	{
		$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

		return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
	}

	public static function stripHtml($html) {
		return trim(strip_tags(html_entity_decode($html)));
	}

	public static function underline($string) {
		return strtolower(str_replace([' '], ['_'], $string));
	}
}