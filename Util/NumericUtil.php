<?php
/**
 * Created by PhpStorm.
 * User: dees91
 * Date: 30/05/16
 * Time: 14:22
 */

namespace AppBundle\Util;


class NumericUtil {

	/**
	 * @param $min
	 * @param $max
	 *
	 * @return mixed
	 */
	public static function randomFloat($min, $max) {
		return $min + rand(0,getrandmax()) / getrandmax() * abs($max - $min);
	}

	/**
	 * @param $string
	 *
	 * @return bool
	 */
	public static function isFloat($string) {
		$floatVal = floatval($string);

		$hasPointer = strpos($string, ',') !== false || strpos($string, '.') !== false;

		return $hasPointer && is_numeric($string) && $floatVal && intval($floatVal) != $floatVal;
	}
}