<?php
/**
 * Created by PhpStorm.
 * User: new
 * Date: 14.04.2016
 * Time: 11:47
 */

namespace AppBundle\Util;


class GraphicUtil
{
    public static function randomColor() {
        $rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
        $color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];

        return $color;
    }

    public static function randomLightColor() {
        return '#' . self::randomColorPart(100) . self::randomColorPart(100) . self::randomColorPart(100);
    }

	public static function greenRedByPercentage($percentage) {
		return self::rgb2hex([
			(255 * $percentage) / 100,
			(255 * (100 - $percentage)) / 100,
			0
		]);
	}

	public static function greenRedByPercentageHsl($percentage) {
		$percentage /= 100;
		$hue = ($percentage) * 120;

		return 'hsl(' . join(',', [$hue, '100%', '50%']) . ')';
	}

    private static function randomColorPart($from = 0, $to = 255) {
        return str_pad( dechex( mt_rand( $from, $to ) ), 2, '0', STR_PAD_LEFT);
    }

	private static function rgb2hex($rgb) {
		$hex = "#";
		$hex .= str_pad(dechex($rgb[0]), 2, "0", STR_PAD_LEFT);
		$hex .= str_pad(dechex($rgb[1]), 2, "0", STR_PAD_LEFT);
		$hex .= str_pad(dechex($rgb[2]), 2, "0", STR_PAD_LEFT);

		return $hex;
	}
}