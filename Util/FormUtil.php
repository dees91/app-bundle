<?php
/**
 * Created by PhpStorm.
 * User: dees91
 * Date: 14/06/16
 * Time: 15:34
 */

namespace AppBundle\Util;


use Symfony\Component\Form\Form;
use Symfony\Component\Form\Util\FormUtil as Util;
use Symfony\Component\HttpFoundation\Request;

class FormUtil {

	/**
	 * @param $form
	 *
	 * @return array
	 */
	public static function getData($form) {
		$data = [];
		if ($form instanceof Form) {
			foreach ($form->all() as $field) {
				if ($field instanceof Form) {
					if (!Util::isEmpty($field->getData())) {
						$data[$field->getName()] = StringUtil::getValuable($field->getData());
					}
				}
			}
		}

		return $data;
	}

	public static function decode(Request $request) {
		if ($request->getMethod() == "POST" && strpos($request->headers->get('Content-Type'), 'application/json') !== false) {
			$data = json_decode($request->getContent(), true);

			return is_array($data) ? $data : [];
		}

		return $request;
	}
}