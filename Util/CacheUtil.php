<?php
/**
 * Created by PhpStorm.
 * User: dees91
 * Date: 31/05/16
 * Time: 11:47
 */

namespace AppBundle\Util;


class CacheUtil {
	public static function buildCacheKey($prefix, array $params = []) {
		return $prefix . '_' . md5(http_build_query($params));
	}
}