<?php
/**
 * Created by PhpStorm.
 * User: dees91
 * Date: 31/05/16
 * Time: 11:48
 */

namespace AppBundle\Util;


class ArrayUtil {

	/**
	 * @param array $array
	 * @param array $keys
	 */
	public static function unsetKeys(array &$array, array $keys) {
		foreach ($array as $key => $value) {
			if (in_array($key, $keys)) {
				unset($array[$key]);
			}
		}
	}

	/**
	 * @param array $array
	 * @param array $keys
	 *
	 * @return array
	 */
	public static function arraySliceAssoc(array $array, array $keys) {
		$simplified = [];
		foreach ($array as $key => $value) {
			$simplified[$key] = md5($value);
		}

		return array_intersect_key($simplified, array_flip($keys));
	}

	/**
	 * @param $search
	 * @param $replace
	 * @param $array
	 *
	 * @return mixed
	 */
	public static function replaceInValues($search, $replace, $array) {
		foreach ($array as $key => $value) {
			$array[$key] = str_replace($search, $replace, $value);
		}

		return $array;
	}

	/**
	 * @param $array
	 * @param $key
	 * @param $item
	 *
	 * @return array
	 */
	public static function pushItem($array, $key, $item) {
		if (!is_array($array)) $array = [];
		if (!isset($array[$key])) $array[$key] = [];

		$array[$key] = array_merge($array[$key], [$item]);

		return $array;
	}

	/**
	 * @param $array
	 * @param $key
	 * @param $item
	 *
	 * @return array
	 */
	public static function putItem($array, $key, $item) {
		if (!is_array($array)) $array = [];

		$array[$key] = $item;

		return $array;
	}
}