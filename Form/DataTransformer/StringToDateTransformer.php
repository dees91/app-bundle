<?php
/**
 * Created by PhpStorm.
 * User: dees91
 * Date: 16/08/16
 * Time: 17:11
 */

namespace AppBundle\Form\DataTransformer;


use Symfony\Component\Form\DataTransformerInterface;

class StringToDateTransformer implements DataTransformerInterface {

	/**
	 * @param \DateTime $value
	 *
	 * @return string
	 */
	public function transform( $value ) {
		if ($value instanceof \DateTime) {
			return $value->format('Y-m-d');
		}

		return $value;
	}

	/**
	 * @param string $value
	 *
	 * @return \DateTime
	 */
	public function reverseTransform( $value ) {
		if (!empty($value)) {
			return \DateTime::createFromFormat('Y-m-d', $value);
		}

		return null;
	}
}