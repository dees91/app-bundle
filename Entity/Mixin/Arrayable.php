<?php
/**
 * Created by PhpStorm.
 * User: dees91
 * Date: 24/08/16
 * Time: 11:01
 */

namespace AppBundle\Entity\Mixin;


trait Arrayable {

	public function __set($property, $value) {
		$methodName = "set" . ucfirst($property);
		if (method_exists($this, $methodName)) {
			call_user_func_array(array($this,$methodName), array($value));
		} else {
			$this->{$property} = $value;
		}
	}

	public function set($data) {
		if (!is_array($data)) {
			return $this;
		}

		foreach ($data as $property => $value) {
			if (property_exists($this, $property)) {
				$this->{$property} = $value;
			}
		}

		return $this;
	}

	/**
	 * returns object properties as an array
	 * @return array
	 */
	public function asArray() {
		$vars = get_object_vars($this);

		return $vars;
	}
}